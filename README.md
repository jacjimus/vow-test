
# Laravel+Vue Test for 


## About the application

A very simple Laravel 8 + Vue 2 + AdminLTE 3 based TEST.


## Tech Specification

Requirements
● As a User, I should be able to create my account (create user with e-mail & password)
○ Include user form validation for e-mail and mis-matching password
● As a User, I should be able to login to my account
● As a User, I should be able to access 2 “credentials” (sections to upload)
● As a User, I should be able to upload at least 2 image files (jpg/png/gif, etc..) to each
credential
● As an Admin, I should be able to see a list of all users in the system and number of files
uploaded for each “credential”
● Please use Laravel & Vue to build the prototype
● Please see attached clickable PDF Wireframe for reference
Expected Outcomes:
● Create & Login to User Account
● Upload 1 or more files categorized by credential
● Ability to list users, credentials and files
● Preferably a repository link (GitHub, BitBucket, GitLab) or zip file of code & DB export
● Development time should take 1-3hrs depending on skills, experience and mastery of
the frameworks

## Installation

- download the application or from git clone with `git clone https://jacjimus@bitbucket.org/jacjimus/vow-test.git`
- `cd vow-test`
- `composer install`
- `cp .env.example .env`
- `create a mysql database`
- Update `.env` and set your database credentials
- `php artisan key:generate`
- `php artisan migrate --seed`
- `php artisan passport:install`
- `npm install`
- `npm run watch`
- `php artisan serve`

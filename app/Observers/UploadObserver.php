<?php

namespace App\Observers;

use App\Models\Uploads;

class UploadObserver
{
    public function saving(Uploads $upload): void
    {
        if (request()->has('filename')) {
            $file_name = time().'_'.request()->file('filename')->getClientOriginalName();
            $file_path = request()->file('filename')->storeAs('', $file_name, 'public');

            $upload->filename = $file_path;
        }
    }
}

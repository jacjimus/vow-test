<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\UploadRequest;
use App\Models\Uploads;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UploadController extends BaseController
{

    protected array $withRelations = ['user'];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    protected string $sortBy = 'id';

    protected string $sortDirection = 'DESC';

    /**
     * Get the class to be used for the CRUD operations.
     *
     * @return string
     */
    protected static function resource(): string
    {
        return Uploads::class;
    }

    /**
     * Get form request to be used when validating the input.
     *
     * @return string
     */
    protected static function resourceFormRequest(): string
    {
        return UploadRequest::class;
    }
    public function users(){
        return $this->sendResponse(User::pluck('username', 'id') , '');
    }

    public function index()
    {
          $type = Auth::user();
         // dd($type);
        if($type->type == 'admin'){
            $resource = Uploads::with('user')->paginate(10);
        }
        else {
           $resource['credential1'] =  Uploads::with('user')->where([['user_id' ,'=', $type->id],
               ['credential' , '=' , 'Credential1']])->paginate(10);
           $resource['credential2'] =  Uploads::with('user')->where([['user_id' ,'=', $type->id],
                                        ['credential' , '=' , 'Credential2']])->paginate(10);
        }
        return $this->sendResponse($resource, 'success');
    }
}

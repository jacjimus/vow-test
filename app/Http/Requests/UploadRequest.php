<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        Auth::user()->type == 'user' ? $this->merge([
            'user_id' => Auth::user()->id,
        ]) : '';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->method() == 'POST' ? $this->saveValidation() : $this->updateValidation();

    }

    /**
     * @return string[]
     */
    private function saveValidation()
    {
        return [
            'user_id' => 'required',
            'credential' => 'required',
            'filename' => 'required|mimes:jpg,jpeg,png|max:2048'
        ];
    }

    private function updateValidation()
    {
        return [
            'user_id' => 'required',
            'credential' => 'required',
            'filename' => 'mimes:jpg,jpeg,png|max:2048'
        ];
    }


}

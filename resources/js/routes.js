export default [
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/uploads', component: require('./components/UserUploads.vue').default },
    { path: '/users', component: require('./components/Users.vue').default },
    { path: '*', component: require('./components/NotFound.vue').default }
];

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Uploads;

class UploadsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Uploads::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'credential' => array_rand(['Credentail1','Credential2']),
            'filename' => $this->faker->image(storage_path('app/public'), 200, 200, null, false),
        ];
    }
}

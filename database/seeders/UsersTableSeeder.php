<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->where('email', 'admin@gmail.com')->delete();

        DB::table('users')->insert([
            'username' => 'jacjimus',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
            'type' => 'admin',
        ]);

        User::factory()
            ->count(10)
            ->hasUploads(5)
            ->create();
    }
}
